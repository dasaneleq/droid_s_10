package com.example.rssfeed;

import nl.matshofman.saxrssreader.RssItem;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ArticleFragment extends Fragment {
	final static String ARG_POSITION = "position";
	final static String ARG_RSS_ITEM = "rss";
	int mCurrentPosition = -1;
	RssItem rssItem;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
		Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
			rssItem = savedInstanceState.getParcelable(ARG_RSS_ITEM);
		}

		return inflater.inflate(R.layout.article_view, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();

		Bundle args = getArguments();
		if (args != null) {
			// Set article based on argument passed in
			updateArticleView(args.getInt(ARG_POSITION), (RssItem) args.getParcelable(ARG_RSS_ITEM));
		} else if (mCurrentPosition != -1) {
			// Set article based on saved instance state defined during onCreateView
			updateArticleView(mCurrentPosition, rssItem);
		}
	}

	public void updateArticleView(int position, RssItem rssItem) {
		TextView title = (TextView) getActivity().findViewById(R.id.title);
		title.setText(rssItem.getTitle());
		TextView article = (TextView) getActivity().findViewById(R.id.article);
		article.setText(Html.fromHtml(rssItem.getFulltext()));
		
		mCurrentPosition = position;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(ARG_POSITION, mCurrentPosition);
		outState.putParcelable(ARG_RSS_ITEM, rssItem);
	}
}