package com.example.rssfeed;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import nl.matshofman.saxrssreader.RssFeed;
import nl.matshofman.saxrssreader.RssItem;
import nl.matshofman.saxrssreader.RssReader;

import org.xml.sax.SAXException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class HeadlinesFragment extends ListFragment {

	private final String URL = "http://k.img.com.ua/rss/ru/technews.xml";
	ArrayList<RssItem> rssItems;

	OnHeadlineSelectedListener mCallback;

	public interface OnHeadlineSelectedListener {
		/** Called by HeadlinesFragment when a list item is selected */
		public void onArticleSelected(int position, RssItem rssItem);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			rssItems = (ArrayList<RssItem>) savedInstanceState.getSerializable("list");
			dataHasBeenLoaded();
		} else {
			new RSSGetter().execute(URL);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception.
		try {
			mCallback = (OnHeadlineSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		mCallback.onArticleSelected(position, rssItems.get(position));
		getListView().setItemChecked(position, true);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (rssItems != null) {
			outState.putSerializable("list", rssItems);
		}
	}

	private void dataHasBeenLoaded() {
		setListAdapter(new RssListAdapter(rssItems));
	}

	private class RSSGetter extends AsyncTask<String, Integer, Boolean> {

		private String errorMessage;

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				URL url = new URL(URL);
				RssFeed feed = RssReader.read(url);
				rssItems = feed.getRssItems();

				return true;
			} catch (MalformedURLException e) {
				errorMessage = "MalformedURLException has no description";
				e.printStackTrace();
			} catch (SAXException e) {
				errorMessage = "I can't parce that dummy XML from feed: " + URL;
				e.printStackTrace();
			} catch (IOException e) {
				errorMessage = "MalformedURLException has no description";
				e.printStackTrace();
			}
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				dataHasBeenLoaded();
			} else {
				showErrorMessage(null == errorMessage ? "Something weird has been happend" : errorMessage);
			}
			super.onPostExecute(result);
		}

	}

	public void showErrorMessage(String string) {
		Toast.makeText(getActivity(), string, Toast.LENGTH_SHORT).show();
	}

	/*
	 * 
	 */
/*-------------------------------------------------------------------------------------------------
 *----------------------------------  Nested classes  ---------------------------------------------
 *---------------------------------------------------------------------------------------------- */

	private class RssListAdapter extends BaseAdapter {

		ArrayList<RssItem> mData;

		public RssListAdapter(ArrayList<RssItem> rss) {
			this.mData = rss;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			RssListViewHolder holder;
			if (convertView == null) {
				convertView = LayoutInflater.from(getActivity())
						.inflate(R.layout.rss_list_item, parent, false);
				holder = new RssListViewHolder(
						(TextView) convertView.findViewById(R.id.tvTitle),
						(TextView) convertView.findViewById(R.id.tvDescr));
				convertView.setTag(holder);
			} else {
				holder = (RssListViewHolder) convertView.getTag();
			}

			holder.title.setText(mData.get(position).getTitle());
			holder.descr.setText(Html.fromHtml(mData.get(position).getDescription()));

			return convertView;
		}

		@Override
		public int getCount() {
			return mData.size();
		}

		@Override
		public RssItem getItem(int position) {
			return mData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private class RssListViewHolder {
			public final TextView title;
			public final TextView descr;

			public RssListViewHolder(TextView title, TextView descr) {
				this.title = title;
				this.descr = descr;
			}
		}
	}
}